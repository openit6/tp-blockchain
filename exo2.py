import Block
import Blockchain

if __name__ == "__main__":
    blockchain = Blockchain()
    db = ["cours", "Graphql", "exam", "note"]

    for i, data in enumerate(db):
        blockchain.minage(Block(data, i))

    for block in blockchain.chain:
        print(block)
