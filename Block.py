from hashlib import sha256


class Block:
    data = None
    hash = None
    nonce = 0
    previous_hash = "0" * 64

    def __init__(self, data, number=0) -> None:
        self.data = data
        self.number = number

    def hash(self):
        return self.updateHash(self.number, self.previous_hash, self.data, self.nonce)

    def __str__(self) -> str:
        return f"block#: {self.number} \n Hash:{self.hash} \n Previous Hash:{self.previous_hash} \n Data:{self.data} \n Nonce:{self.data}"

    def updateHash(self, *args):
        hashing_text = ""
        h = sha256()

        for arg in args:
            hashing_text += str(arg)

        h.update(hashing_text.encode("utf-8"))
        return h.hexdigest()
