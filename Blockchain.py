class Blockchain:
    difficulty = 2

    def __init__(self):
        self.chain = []

    def add_block(self, block):
        self.chain.append(
            {
                "hash": block.hash(),
                "previous": block.previous_hash,
                "number": block.number,
                "data": block.data,
                "nonce": block.nonce,
            }
        )

    def minage(self, block):
        try:
            block.previous_hash = self.chain[-1].get("hash")
        except IndexError:
            pass
        while True:
            if block.hash()[: self.difficulty] == "0" * self.difficulty:
                self.add_block(block)
                break
            else:
                block.nonce += 1

    def isValid(self):
        for i in range(1, len(self.chain)):
            _previous = self.chain[i - 1].hash()
            _current = self.chain[i].hash()
